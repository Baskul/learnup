package Task03;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static final int ARRAY_SIZE = 10;
    public static final int MAX_STEPS = 20;

    public static void main(String[] args) {
        int[][] state = new int[ARRAY_SIZE][ARRAY_SIZE];

        state[0][1] = 1;
        state[1][2] = 1;
        state[2][0] = 1;  //глайдер
        state[2][1] = 1;
        state[2][2] = 1;


        int steps = 1 + new Random().nextInt(MAX_STEPS - 1);
        //int steps = 120;
        for (int i = 0; i < steps; i++) {
            state = generateNextStep(state);
        }

        for (int i = 0; i < state.length; i++) {
            for (int j = 0; j < state[i].length; j++) {
                System.out.print(state[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] generateNextStep(int[][] state) {
        int[][] new_state = new int[state.length][state[0].length];
        for (int i = 0; i < state.length; i++) {
            for (int j = 0; j < state[i].length; j++) {
                int count = 0;
                for (int k = -1; k <= 1; k++) { // проход по окружающим ячейкам
                    for (int l = -1; l <= 1; l++) {
                        if (k == 0 && l == 0) {
                            continue;
                        } else if (i + k >= 0 && i + k < state.length && // проверка индексов на выход за диапазон
                                j + l >= 0 && j + l < state[i].length) {
                            count += state[i + k][j + l];
                        } else {
                            int ind1 = i + k;
                            int ind2 = j + l;
                            if (ind1 < 0) ind1 = state.length - 1;
                            if (ind1 >= state.length) ind1 = 0;
                            if (ind2 < 0) ind2 = state[0].length - 1;
                            if (ind2 >= state[0].length) ind2 = 0;

                            count += state[ind1][ind2];
                        }
                    }
                }
                if (state[i][j] == 0 && count == 3) { // мертвая клетка оживает
                    new_state[i][j] = 1;
                }
                if (state[i][j] == 1) { // живая клетка
                    if (count < 2 || count > 3) { // умирает
                        new_state[i][j] = 0;
                    } else { // живет
                        new_state[i][j] = 1;
                    }
                }
            }
        }
        return new_state;
    }
}

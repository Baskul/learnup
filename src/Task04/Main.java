package Task04;

import Task04.Carriages.*;
import Task04.Employees.*;

public class Main {

    public static void main(String[] args) {

        RailwayCarriage[] carriages = new RailwayCarriage[5];

        carriages[0] = new CompartmentCarriage(
                new Conductor("Василий", 31, 21000),
                300);
        carriages[1] = new SVCarriage(
                new Conductor("Генадий", 27, 23000),
                400);
        carriages[2] = new PlatskartCarriage(
                new Conductor("Любовь", 52, 33000),
                500);
        FreightManager freight_manager = new FreightManager("Глеб", 34, 60000);
        carriages[3] = new FreightCarriage(
                freight_manager,
                2500,
                "Уголь",
                2000);
        carriages[4] = new FreightCarriage(
                freight_manager,
                1500,
                "Газ",
                1000);
        ((FreightCarriage) carriages[4]).addFreight(200);


        LocomotiveDriver locomotive_driver = new LocomotiveDriver("Михаил", 54, 40000, 12);
        Locomotive locomotive = new Locomotive(
                locomotive_driver,
                120, 600);
        locomotive.addFuel(100);

        TrainManager train_manager = new TrainManager("Аркадий", 45, 50000);
        Station Vladik = new Station("Владикавказ");
        Vladik.setPoliceWorker(new StationPoliceWorker("Дмитрий", 27, 41000));
        Train train = new Train(
                locomotive,
                train_manager,
                carriages,
                Vladik);

        Station Moscow = new Station("Москва");
        train.move(Moscow, 9228);
        Station st = train.getCurrent_place();
        System.out.println(st.getName());
    }

}

package Task04;


import Task04.Employees.StationPoliceWorker;

public class Station {
    private String name;
    private StationPoliceWorker policeWorker;

    public Station(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StationPoliceWorker getPoliceWorker() {
        return policeWorker;
    }

    public void setPoliceWorker(StationPoliceWorker policeWorker) {
        this.policeWorker = policeWorker;
        policeWorker.setStation(this);
    }
}

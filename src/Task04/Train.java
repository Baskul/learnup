package Task04;

import Task04.Carriages.RailwayCarriage;
import Task04.Employees.TrainManager;
import Task04.Employees.TrainWorker;

public class Train {
    private Locomotive locomotive;
    private TrainWorker train_manager;
    private RailwayCarriage[] carriages;
    private Station current_place;


    public Train(Locomotive locomotive, TrainManager train_manager, RailwayCarriage[] carriages, Station current_place) {
        this.locomotive = locomotive;
        locomotive.setTrain_manager(train_manager);
        train_manager.setTrain(this);
        this.train_manager = train_manager;
        this.carriages = carriages;
        this.current_place = current_place;
    }

    public Locomotive getLocomotive() {
        return locomotive;
    }

    public RailwayCarriage[] getCarriages() {
        return carriages;
    }

    public Station getCurrent_place() {
        return current_place;
    }

    public void setTrain_manager(TrainWorker train_manager) {
        this.train_manager = train_manager;
    }



    public void setCarriages(RailwayCarriage[] carriages) {
        this.carriages = carriages;
    }

    public void move(Station destination, double distance) {
        double weight = locomotive.getWeight();
        for (RailwayCarriage carriage : carriages) {
            weight += carriage.getWeight();
        }

        if (locomotive.isAbleToMove(distance, weight)) {
            current_place = destination;
        }
    }
}

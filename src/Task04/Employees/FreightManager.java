package Task04.Employees;

import Task04.Carriages.FreightCarriage;

public class FreightManager extends TrainWorker{
    private FreightCarriage carriage;

    public FreightManager(String name, int age, double salary) {
        super(name, age, salary);
    }

    public FreightCarriage getCarriage() {
        return carriage;
    }

    public void setCarriage(FreightCarriage carriage) {
        this.carriage = carriage;
    }
}

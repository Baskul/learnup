package Task04.Employees;

import Task04.Carriages.PassengerCarriage;

public class Conductor extends TrainWorker{
    private PassengerCarriage carriage;

    public Conductor(String name, int age, double salary) {
        super(name, age, salary);
    }

    public PassengerCarriage getCarriage() {
        return carriage;
    }

    public void setCarriage(PassengerCarriage carriage) {
        this.carriage = carriage;
    }



}

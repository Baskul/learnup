package Task04.Employees;

import Task04.Station;

public class StationPoliceWorker extends TrainWorker{
    private Station station;

    public StationPoliceWorker(String name, int age, double salary) {
        super(name, age, salary);
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }
}

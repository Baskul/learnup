package Task04.Employees;

import Task04.WeightGainable;

public abstract class TrainWorker implements WeightGainable {
    private String name;
    private int age;
    private double weight;
    private double salary;

    public TrainWorker(String name, int age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public void addWeight(double weight) {
        this.weight += weight;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}

package Task04.Employees;

import Task04.Locomotive;

public class LocomotiveDriver extends TrainWorker{
    private Locomotive locomotive;
    private double driving_experience;

    public LocomotiveDriver(String name, int age, double salary, double driving_experience) {
        super(name, age, salary);
        this.driving_experience = driving_experience;
    }

    public Locomotive getLocomotive() {
        return locomotive;
    }

    public void setLocomotive(Locomotive locomotive) {
        this.locomotive = locomotive;
    }
}

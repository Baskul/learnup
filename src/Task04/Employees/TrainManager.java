package Task04.Employees;

import Task04.Train;

public class TrainManager extends TrainWorker {
    private Train train;

    public TrainManager(String name, int age, double salary) {
        super(name, age, salary);
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }
}

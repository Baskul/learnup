package Task04;

import Task04.Employees.LocomotiveDriver;
import Task04.Employees.TrainWorker;

public class Locomotive {
    private LocomotiveDriver locomotive_driver;
    private TrainWorker train_manager;

    private double engine_power;
    private double fuel_quantity;
    private double weight;

    public Locomotive(LocomotiveDriver locomotive_driver, double engine_power, double weight) {
        locomotive_driver.setLocomotive(this);
        this.locomotive_driver = locomotive_driver;
        this.engine_power = engine_power;
        this.weight = weight;
    }

    public TrainWorker getTrain_manager() {
        return train_manager;
    }

    public void setTrain_manager(TrainWorker train_manager) {
        this.train_manager = train_manager;
    }

    public boolean isAbleToMove(double distance, double weight) {
        //return distance - engine_power * fuel_quantity / weight > 0; // реализовно в исключениях
        if (fuel_quantity >= 10) {
            fuel_quantity -= 10;
            return true;
        }
        else return false;
    }

    public double getEngine_power() {
        return engine_power;
    }

    public double getWeight() {
        return weight;
    }

    public void addFuel(double quantity){
        fuel_quantity += quantity;
    }
}

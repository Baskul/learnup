package Task04.Carriages;

import Task04.Employees.Conductor;

public class SVCarriage extends PassengerCarriage {
    public SVCarriage(Conductor carriage_manager, double weight) {
        super(carriage_manager, weight, SV_SEAT_NUMBER);
    }
}

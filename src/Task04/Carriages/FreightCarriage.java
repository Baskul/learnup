package Task04.Carriages;

import Task04.Employees.FreightManager;

public class FreightCarriage extends RailwayCarriage {
    private String cargo_description;
    private double cargo_weight;

    public FreightCarriage(FreightManager freight_manager, double weight, String cargo_description, double cargo_weight) {
        super(freight_manager, weight);
        freight_manager.setCarriage(this);
        this.cargo_description = cargo_description;
        this.cargo_weight = cargo_weight;
    }

    public String getCargo_description() {
        return cargo_description;
    }

    public double getCargo_weight() {
        return cargo_weight;
    }

    public void addFreight(double weight) {
        this.weight  += weight;
        cargo_weight += weight;
    }
}

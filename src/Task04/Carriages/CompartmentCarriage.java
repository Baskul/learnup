package Task04.Carriages;

import Task04.Employees.Conductor;

public class CompartmentCarriage extends PassengerCarriage {
    public CompartmentCarriage(Conductor carriage_manager, double weight) {
        super(carriage_manager, weight, COMPARTMANT_SEAT_NUMBER);
    }
}

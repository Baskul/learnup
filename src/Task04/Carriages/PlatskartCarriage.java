package Task04.Carriages;

import Task04.Employees.Conductor;

public class PlatskartCarriage extends PassengerCarriage {

    public PlatskartCarriage(Conductor carriage_manager, double weight) {
        super(carriage_manager, weight, PLATSKART_SEAT_NUMBER);
    }
}

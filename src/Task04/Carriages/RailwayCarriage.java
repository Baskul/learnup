package Task04.Carriages;

import Task04.Employees.TrainWorker;
import Task04.WeightGainable;

public abstract class RailwayCarriage implements WeightGainable {
    private TrainWorker carriage_manager;
    protected double weight;

    public RailwayCarriage(TrainWorker carriage_manager, double weight) {
        this.carriage_manager = carriage_manager;
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public void addWeight(double weight) {
        this.weight += weight;
    }

    public TrainWorker getCarriage_manager() {
        return carriage_manager;
    }

    public void setCarriage_manager(TrainWorker carriage_manager) {
        this.carriage_manager = carriage_manager;
    }
}

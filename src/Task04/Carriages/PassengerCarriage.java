package Task04.Carriages;

import Task04.Employees.Conductor;

public abstract class PassengerCarriage extends RailwayCarriage {
    protected int seat_number;
    protected static final int PLATSKART_SEAT_NUMBER = 54;
    protected static final int COMPARTMANT_SEAT_NUMBER = 36;
    protected static final int SV_SEAT_NUMBER = 18;

    public PassengerCarriage(Conductor conductor, double weight, int seat_number) {
        super(conductor, weight);
        conductor.setCarriage(this);
        this.seat_number = seat_number;
    }

    public int getSeat_number() {
        return seat_number;
    }
}
